using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonresponse : MonoBehaviour
{
	

    //Changes the from Scene : gamemenu to Scene : gameplay
    public void GamePlayScene() {
mechanics.resume = false;

		SceneManager.LoadScene("GamePlay");
    
    }

    public void Resume() {
        mechanics.resume = true;
        SceneManager.LoadScene("GamePlay");
    }




    //Changes the from Scene : gamemenu to Scene : Stats Scene
    public void StatsScene() {
    	
        SceneManager.LoadScene("StatsScene");
    
    }


    
}
