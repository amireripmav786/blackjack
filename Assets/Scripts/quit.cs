using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class quit : MonoBehaviour
{	
	//Quits the game on clicking the Quit button in Game menu.
   	public void QuitGame ( ) {
	
		Application.Quit();

	}
}
