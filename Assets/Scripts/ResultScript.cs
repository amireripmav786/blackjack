using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

using System.IO;


//This Script shows the stats of the current game played. It saves the stats of the current game played in a JSON file.
public class ResultScript : MonoBehaviour
{
   
   static int dealerMoney = mechanics.dealerMoney;
   static int playerMoney = mechanics.playerMoney;
   static int gameWon = mechanics.gameWon;
   static int totalHands = mechanics.totalHands;
   static int moneyWon = mechanics.moneyWon;
   static int moneyLost = mechanics.moneyLost;

   public Text resultText;
   public Text handsPlayedText;
   public Text handsLostText;
   public Text handsWonText;
   public Text moneyLostText;
   public Text moneyWonText;

   

   //It checks whether the player has won the game or not.
   //Shows a notification accordingly.
   //Calls a function (SaveByJson()) which saves data in the JSON file.
   //Calls a function (ShowStats()) which reflects the statistics of current game in the UI.
   void Start() {

   dealerMoney = mechanics.dealerMoney;
   playerMoney = mechanics.playerMoney;
   gameWon = mechanics.gameWon;
   totalHands = mechanics.totalHands;
   moneyWon = mechanics.moneyWon;
   moneyLost = mechanics.moneyLost;

      SaveByJson();

      if(dealerMoney>playerMoney) { 

            resultText.text = "WOOPS.... YOU LOST :(";
      }
      else if(playerMoney>dealerMoney) {

            resultText.text = "VOILA!!! YOU WIN :)";
      }

      ShowStats();
   
   }




   //This functions shows the statistics of the current game played 
   //such as hands played, hands won, money won, money lost on the UI.
   void ShowStats() { 
      
      handsPlayedText.text = String.Concat ( handsPlayedText.text, "\n", totalHands.ToString() );

      handsWonText.text = String.Concat ( handsWonText.text, "\n", gameWon.ToString() );
      handsLostText.text = String.Concat ( handsLostText.text, "\n",  (totalHands-gameWon).ToString() );

 

   
      
      moneyWonText.text = String.Concat ( moneyWonText.text, "\n",  moneyWon.ToString() );
      moneyLostText.text = String.Concat ( moneyLostText.text, "\n", moneyLost.ToString() );

   }





   //This function quits the game on - clicking the quit button in the scene.

   public void MainMenu() {

         SceneManager.LoadScene("gamemenu");

   }

   //This is the class which has all the features we need to save as instance variables.
   class SaveData {
   
    public int handsPlayed;
    public int handsWon;
    public int handsLost;
    public int moneyWon;
    public int moneyLost;
   
   }


   
   //It creates a C# Object from the currrent stats of the game.
   SaveData createSaveObject() {
      
      SaveData saveDataObject = new SaveData();
      
      saveDataObject.handsPlayed = totalHands;
      saveDataObject.handsWon = gameWon;
      saveDataObject.handsLost = totalHands - gameWon;
      saveDataObject.moneyWon = moneyWon;
      saveDataObject.moneyLost = moneyLost;

      return saveDataObject;
   }

   

   //This functions writes/saves the data into a JSON File.
   //First it reads the data already present in the file and stores a string.
   //It convers the C# Object created by the current stats into a JSON string.
   //Then it appends the data already present with the JSON string.
   //Then it writes this string in the file.
   private void SaveByJson() {
      
      StreamReader sr = new StreamReader(Application.persistentDataPath+"/JSONData.text");
      string fileString = sr.ReadToEnd();
      sr.Close();
      
      SaveData save = createSaveObject();

      string JsonString = string.Concat(JsonUtility.ToJson(save), "^", fileString);

      StreamWriter sw = new StreamWriter(Application.persistentDataPath+"/JSONData.text");
      sw.Write(JsonString);
      sw.Close();

      Debug.Log("DATA SAVED");
   }

    
}
