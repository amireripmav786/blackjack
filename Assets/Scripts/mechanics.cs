using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using System.IO;

public class mechanics : MonoBehaviour
{

    public static bool resume = false;

    int numberOfCards = 52;
    float factor = 1.0f; 
    public Sprite[] cardSprites;
    int[] cardValues = new int[52];

    public Image[] playerHits;
    int playerHitCount=0;

    public Image[] dealerHits;
    int dealerHitCount=0;
    public Text dealerMoneyText;
    public Text playerMoneyText;
    bool betRaised = false;

    
    public Image playerCardOne;    
    public Image playerCardTwo; 
    public Image dealerCardOne;
    public Image dealerCardTwo;
    
    
    public Text notifications;
    public Text betMoneyText;
    
    int playerCardsTotal=0;
    int dealerCardsTotal=0;
    public static int dealerMoney =2000;
    public static int playerMoney =600;
    public static int gameWon =0;
    public static int totalHands =0;
    public static int moneyWon=0;
    public static int moneyLost=0;
    int betMoney = 200;
    Sprite dealerHiddenCard;
    Sprite dealerInitialHiddenCard;
    int counter = 0;
    

    //This  function shuffles the deck in O(n) using Fisher - Yates Shuffle.
    void randomize() {
        

        System.Random r_obj = new System.Random();
         
        for (int currIndex = numberOfCards - 1; currIndex > 0; currIndex--) {
             
            int randomIndex = r_obj.Next(0, currIndex+1);
             
            Sprite temp = cardSprites[currIndex];
            cardSprites[currIndex] = cardSprites[randomIndex];
            cardSprites[randomIndex] = temp;


            int tempValue = cardValues[currIndex];
            cardValues[currIndex] = cardValues[randomIndex];
            cardValues[randomIndex] = tempValue;
        }

       
        
    }

    

    //This function initialises the deck of card.
    void intialise(){

        int index=0;
        for(int i=1;i<=4;i++)
        {
            int currValue = 1;
            for(int j=1;j<=13;j++){
                cardValues[index] = currValue;
                index = index + 1;
                currValue  = currValue + 1;
                if(currValue>=10){
                    currValue = 10;
                } 
            }
             
        }

    }




    //This function assigns a given quantity of card to player or dealer according to the role given.
    void assign(int quantity, string role){

        if(role=="player"){
            

         
                while(quantity>0) {

                    int currValue = cardValues[counter];
                    playerCardsTotal = playerCardsTotal + currValue;

                    if(quantity==2)
                    playerCardOne.sprite = cardSprites[counter];
                    else
                    playerCardTwo.sprite = cardSprites[counter];
             
                    if(playerCardsSumCheck()==false){
                    return;
                    }

           
                    counter = counter + 1;
                    if(counter == numberOfCards){
                        randomize();
                        counter=0;
                    }
                
                    quantity = quantity - 1;
            
                }
            
        }
        else{


            while(quantity>0) {

            int currValue = cardValues[counter];
            dealerCardsTotal = dealerCardsTotal + currValue;
            
            if(quantity==2)
            dealerCardOne.sprite = cardSprites[counter];
            else if(quantity==1)
            dealerHiddenCard = cardSprites[counter];
            
            counter = counter + 1;
            
            if(counter == numberOfCards){
                randomize();
                counter=0;
            }
            
            quantity = quantity - 1;
            
            }

        }


    }




    //This function checks whether while assigning the card to a player, there has been a Bust.
    bool playerCardsSumCheck(){
        if(playerCardsTotal>21){

            reveal();
            notifications.text = "It is a bust:( Raise a new bet";

            
            Invoke("lose",1.0f);

            return false;
        }
        else{
            return true;
        }
    }




    



    //When a player clicks on Hit, this function is called.
    //Only allowed when a bet has been raised.
    //It assigns one card to player.
    public void hit() {
        
        if( betRaised == false ) {
            return;
        }
        Debug.Log("HITTING");

        int currValue = cardValues[counter];
        playerCardsTotal = playerCardsTotal + currValue;
        playerHits[playerHitCount].sprite = cardSprites[counter];
       
        playerHits[playerHitCount].gameObject.SetActive(true);
        playerHitCount = playerHitCount + 1;
        if(playerCardsSumCheck()==false) {
                    
                    return;
        
        }

        counter = counter + 1;
        if(counter == numberOfCards){
            randomize();
            counter=0;
        }
       
    }






    //When a player clicks on stand this function is called.
    //Only allowed when a bet has been raised.
    //It checks who has won this hand according to their respective card sum.
    //According to the reasult win() and lose() functions are called.
    public void stand( ){
        
        if(betRaised == false) {
            return;
        }
        
        reveal();

        while(dealerCardsTotal<17) {
            dealerCardsTotal = dealerCardsTotal + cardValues[counter];
            dealerHits[dealerHitCount].sprite = cardSprites[counter];
            dealerHits[dealerHitCount].gameObject.SetActive(true);

            dealerHitCount = dealerHitCount + 1;
            counter = counter + 1;
            if(counter == numberOfCards){
                randomize();
                counter = 0;
            }

        }




        resultOut();
    
    }



    void resultOut() {

        if(playerCardsTotal>=dealerCardsTotal || dealerCardsTotal>21){
            
            if(playerCardsTotal == dealerCardsTotal) {
                factor = 1.5f;
            }
            
            notifications.text = "You won :) Raise a new bet";
            Invoke("win",1.0f);
        }
        else{
            notifications.text = "You lost :( Raise a new bet";
            Invoke("lose", 1.0f);
        }
    }

    //When a player clicks stand, then this function reveals the card of the Dealer.
    void reveal (){
        dealerCardTwo.sprite = dealerHiddenCard;
    }




    //This function does the required modifications when player loses a hand.
    void lose(){
        playerMoney = playerMoney - (int)(betMoney * factor);
        dealerMoney = dealerMoney + (int)(betMoney * factor);
        factor = 1.0f;

        moneyLost = moneyLost + (int)(betMoney * factor);

        playerMoneyText.text = playerMoney.ToString();
        dealerMoneyText.text = dealerMoney.ToString();

        betRaised = false;
        betMoneyText.text="";
        if(playerMoney<=0){
            GameOver();
        }
        
    }



    //This function does the required modifications when player wins a hand.
    void win(){
        playerMoney = playerMoney + (int)(betMoney * factor);
        dealerMoney = dealerMoney - (int)(betMoney * factor);
        factor = 1.0f;
        moneyWon = moneyWon + (int)(betMoney * factor);

        playerMoneyText.text = playerMoney.ToString();
        dealerMoneyText.text = dealerMoney.ToString();

        gameWon = gameWon + 1;

        betRaised = false;
        betMoneyText.text="";

        if(dealerMoney<=0){
            GameOver();
        }
    }




    //This function starts a hand.
    //assigns 2 cards to dealer and 2 to player.   
    public void Round(){
        
        for(int i=0;i<playerHits.Length;i++){
            playerHits[i].gameObject.SetActive(false);
        }
        for(int i=0;i<dealerHits.Length;i++){
            dealerHits[i].gameObject.SetActive(false);
        }
        notifications.text="";

        playerCardsTotal = 0;
        dealerCardsTotal = 0;
        playerHitCount = 0;
        dealerHitCount = 0;
        playerCardOne.sprite =null;
        playerCardTwo.sprite = null;
        dealerCardOne.sprite =null;
        dealerCardTwo.sprite = dealerInitialHiddenCard;
        
        

        
        assign(2,"dealer");

        
        assign(2, "player");

        totalHands = totalHands + 1;

    }


    //when either of them run out of money, the game gets over and scene switches to the stat screen.
    void GameOver () {
        Debug.Log(gameWon+" "+totalHands+" "+dealerMoney+" "+playerMoney);
        
            SceneManager.LoadScene("ResultScene");
    }


    

    //Intitialses the deck.
    //Shuffles the deck.
    void Start() {
        if(resume==false){
         NewGame();
        }
        else{
            LoadGame();
        }
        
        
        Debug.Log(gameWon+" "+totalHands+" "+dealerMoney+" "+playerMoney+" "+moneyWon+" "+moneyLost);
        dealerInitialHiddenCard = dealerCardTwo.sprite;

       
        intialise();
        randomize();    
        
        
    }


    void NewGame() {
        dealerMoney =2000;
        playerMoney =600;
        gameWon =0;
        totalHands =0;
        moneyWon=0;
        moneyLost=0;
         for(int i=0;i<playerHits.Length;i++){
            playerHits[i].gameObject.SetActive(false);
        }


        for(int i=0;i<dealerHits.Length;i++){
            dealerHits[i].gameObject.SetActive(false);
        }

    }
   
    void LoadGame(){
        StreamReader sr = new StreamReader(Application.persistentDataPath+"/JsonGameState.text");
        string JsonString = sr.ReadToEnd();
        sr.Close();
        
        GameStates curr = JsonUtility.FromJson<GameStates>(JsonString);

        dealerMoney=curr.dealerMoneyData;
        dealerMoneyText.text = dealerMoney.ToString();

        playerMoney=curr.playerMoneyData;
        playerMoneyText.text = playerMoney.ToString();

        counter=curr.counterData;
        
        playerHitCount=curr.playerHitCountData;
        dealerHitCount=curr.dealerHitCountData;
        
        for(int i=playerHitCount;i<playerHits.Length;i++){
            playerHits[i].gameObject.SetActive(false);
            
        }

        for(int i=0;i<playerHitCount;i++){
            playerHits[i].gameObject.SetActive(true);
            
        }

        for(int i=dealerHitCount;i<dealerHits.Length;i++){
            
            dealerHits[i].gameObject.SetActive(false);
            
        }


        for(int i=0;i<dealerHitCount;i++){
           dealerHits[i].gameObject.SetActive(true);
            
        }

        
        
        for(int i=0;i<curr.cardSpritesData.Length;i++) {
            cardSprites[i] = curr.cardSpritesData[i];
            cardValues[i] =  curr.cardValueData[i];
        }

        gameWon=curr.gameWonData;
        totalHands=curr.totalHandsData;
        moneyWon=curr.moneyWonData;
        moneyLost=curr.moneyLostData;
        betRaised=curr.betRaisedData;

        if(betRaised){
            betMoneyText.text = betMoney.ToString();
        }
        playerCardsTotal=curr.playerCardsTotalData;
        dealerCardsTotal=curr.dealerCardsTotalData;

        playerCardOne.sprite=curr.playerCardOneData;
        playerCardTwo.sprite=curr.playerCardTwoData;
        dealerCardOne.sprite=curr.dealerCardOneData;
        dealerCardTwo.sprite=curr.dealerCardTwoData;
        

        for(int i=0;i<dealerHits.Length;i++){
            dealerHits[i].sprite = curr.dealerHitsData[i];
            playerHits[i].sprite = curr.playerHitsData[i];
        }


        notifications.text = curr.notificationsTextData;
        factor= curr.factorData;

        
    }

    class GameStates {
        public int dealerMoneyData;
         public int playerMoneyData;
         public int counterData;
         public int playerHitCountData;
         public int dealerHitCountData;
         public bool[] hitDealerState = new bool[9];
         public bool[] hitPlayerState = new bool[9];
         public Sprite[] cardSpritesData = new Sprite[52];
         public int[] cardValueData = new int[52];

         public int gameWonData;
         public int totalHandsData;
         public int moneyWonData;
         public int moneyLostData;
        public bool betRaisedData;
        public String notificationsTextData;
         public int playerCardsTotalData;
     public int dealerCardsTotalData;
        public Sprite playerCardOneData;
        public Sprite playerCardTwoData;
        public Sprite dealerCardOneData;
        public Sprite dealerCardTwoData;
        public Sprite[] dealerHitsData= new Sprite[9];
        public Sprite[] playerHitsData = new Sprite[9];
        public float factorData;
    }

    public void SaveData() {
        GameStates curr = new GameStates();
        curr.dealerMoneyData=dealerMoney;
        curr.playerMoneyData=playerMoney;
        curr.counterData=counter;
        curr.playerHitCountData=playerHitCount;
        curr.dealerHitCountData=dealerHitCount;
        
          for(int i=0;i<curr.cardSpritesData.Length;i++) {
            curr.cardSpritesData[i] = cardSprites[i];
            curr.cardValueData[i] = cardValues[i];
        }


        curr.gameWonData=gameWon;
        curr.totalHandsData=totalHands;
        curr.moneyWonData=moneyWon;
        curr.moneyLostData=moneyLost;
        curr.betRaisedData=betRaised;

        curr.playerCardsTotalData=playerCardsTotal;
        curr.dealerCardsTotalData=dealerCardsTotal;

        curr.playerCardOneData=playerCardOne.sprite;
        curr.playerCardTwoData=playerCardTwo.sprite;
        curr.dealerCardOneData=dealerCardOne.sprite;
        curr.dealerCardTwoData=dealerCardTwo.sprite;
        

        for(int i=0;i<dealerHits.Length;i++){
            curr.dealerHitsData[i]=dealerHits[i].sprite;
            curr.playerHitsData[i]=playerHits[i].sprite;
        }
        curr.factorData=factor;
        curr.notificationsTextData= notifications.text;
   
      
      string JsonString = JsonUtility.ToJson(curr);

      StreamWriter sw = new StreamWriter(Application.persistentDataPath+"/JsonGameState.text");
      sw.Write(JsonString);
      sw.Close();

      Debug.Log("DATA SAVED");
      SceneManager.LoadScene("gamemenu");
    }
    //This function raises a bet.
    //Then starts a new round.
    public void RaiseBet() {
        
        if(betRaised==true)
            return;

        betRaised = true;

        betMoneyText.text = "BET RAISED : 200";
        Round();
    }

   
}
