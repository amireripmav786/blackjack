using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using System.IO;

//This script reads the data from JSON file and displays on the UI.

public class LoadStats : MonoBehaviour
{


    public Text handsPlayedText;
    public Text handsLostText;
    public Text handsWonText;
    public Text moneyWonText;
    public Text moneyLostText;
    
    string JsonString;



    //This function calls two functions responsible to fetch data from JSON file and display in UI.
    void Start() {

        LoadData(); 
        ShowData();
    }


    //This function switches the screen back to the GameMenu
    public void MainMenu() {

         SceneManager.LoadScene("gamemenu");

    }



    //We have fetched the data from file in JSONString.
    //This functions takes this string.
    //Splits them and generates an array of string. Each element of array consist of a JSON object which represents a stat.
    //Then each JSON object is converted into C# Object.
    //Then, the respective data from the C# Object is shown in the UI by changing text of the UI Components.
    void ShowData() {
        string[] eachStat = JsonString.Split('^');

        for( int i=0; i<eachStat.Length-1; i++){

            Debug.Log(eachStat[i]);

            SaveData currStat = JsonUtility.FromJson<SaveData>(eachStat[i]);
        
            handsPlayedText.text = string.Concat(handsPlayedText.text, currStat.handsPlayed, "\n");
            handsLostText.text = string.Concat(handsLostText.text, currStat.handsLost, "\n");
            handsWonText.text = string.Concat(handsWonText.text, currStat.handsWon, "\n");
            moneyWonText.text = string.Concat(moneyWonText.text, currStat.moneyWon, "\n");
            moneyLostText.text = string.Concat(moneyLostText.text, currStat.moneyLost, "\n");


        }
    }



    //This functions reads the JSON file till the end and saves in a string(JSON String).
    void LoadData() {

        StreamReader sr = new StreamReader(Application.persistentDataPath+"/JSONData.text");

        JsonString = sr.ReadToEnd();
        sr.Close();
    
    }




    //The skeleton class which has features as instance variables which we need to save.
    class SaveData {
   
    public int handsPlayed;
    public int handsWon;
    public int handsLost;
    public int moneyWon;
    public int moneyLost;
   
   }


}
